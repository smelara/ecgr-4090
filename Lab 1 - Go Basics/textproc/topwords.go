// Find the top K most common words in a text document.
// Input path: location of the document, K top words
// Output: Slice of top K words
// For this excercise, word is defined as characters separated by a whitespace

// Note: You should use `checkError` to handle potential errors.

package textproc

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

func TopWords(path string, K int) []WordCount {
	file, err := os.Open(path) //open the file
	checkError(err)            //check for errors
	defer file.Close()         // This defer function ensures the file is closed when the function exits

	wordCounts := make(map[string]int) // Creates map to store the word count

	//Using scanner to read from file and using "split" to split the input into words
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	//Loop through each word in passage and convert to lower case and update word count in our map
	for scanner.Scan() {
		word := strings.ToLower(scanner.Text())
		wordCounts[word]++
	}

	//Check for errors during scanning
	checkError(scanner.Err())

	//Create a slice to store WordCount structures
	var wcSlice []WordCount
	//Converting the map of word counts to slices of WordCount structures
	for word, count := range wordCounts {
		wcSlice = append(wcSlice, WordCount{Word: word, Count: count})
	}

	sortWordCounts(wcSlice)

	//If K is grreater than total # of unique words, set K to be equal to total 3 of words
	if K > len(wcSlice) {
		K = len(wcSlice)
	}
	//Return the top K words and the counts of them
	return wcSlice[:K]
}

//--------------- DO NOT MODIFY----------------!

// A struct that represents how many times a word is observed in a document
type WordCount struct {
	Word  string
	Count int
}

// Method to convert struct to string format
func (wc WordCount) String() string {
	return fmt.Sprintf("%v: %v", wc.Word, wc.Count)
}

// Helper function to sort a list of word counts in place.
// This sorts by the count in decreasing order, breaking ties using the word.

func sortWordCounts(wordCounts []WordCount) {
	sort.Slice(wordCounts, func(i, j int) bool {
		wc1 := wordCounts[i]
		wc2 := wordCounts[j]
		if wc1.Count == wc2.Count {
			return wc1.Word < wc2.Word
		}
		return wc1.Count > wc2.Count
	})
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
